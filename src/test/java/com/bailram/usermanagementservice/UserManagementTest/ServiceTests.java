package com.bailram.usermanagementservice.UserManagementTest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import com.bailram.usermanagementservice.domain.entity.User;
import com.bailram.usermanagementservice.repository.UserRepository;
import com.bailram.usermanagementservice.service.UserManagementService;

@ExtendWith(MockitoExtension.class)
@RunWith(MockitoJUnitRunner.class)
@MockitoSettings(strictness=Strictness.LENIENT)
@SpringBootTest
public class ServiceTests {
	@Mock
	@Autowired
	private UserRepository userRepository;
	
	@InjectMocks
	@Autowired
	private UserManagementService userManagementService;
	
	private User user;
	
	@Before
	public void init() {
		user.setUserId(null);
		user.setAddress("jl jonggo");
		user.setFullName("Danill YUMI");
		user.setRole("admin");
	}
	
	@Test
	void shouldUserShouldNotNull() {
//		User user = new User(6L, "Danilll Par", "sdasd", "dsadsad");
//		User save = userManagementService.saveUser(user);
//		System.out.print(save.toString());
//		Assert.assertNotNull(userManagementService.getUser(4L));
//		Assert.assertEquals(user, save);
//		Assert.assertNotNull(save);
		when(userManagementService.saveUser(user)).thenReturn(new User());
	}
	
	@Test
	void tesing () {
		Assert.assertEquals(1, 1);
	}
}
