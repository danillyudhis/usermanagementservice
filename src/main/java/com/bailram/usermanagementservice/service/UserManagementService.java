package com.bailram.usermanagementservice.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bailram.usermanagementservice.domain.dto.UserDto;
import com.bailram.usermanagementservice.domain.dto.UserRequest;
import com.bailram.usermanagementservice.domain.dto.UserResponse;
import com.bailram.usermanagementservice.domain.entity.User;
import com.bailram.usermanagementservice.repository.UserRepository;

@Service
public class UserManagementService {
	@Autowired
	private UserRepository userRepository;
	
	public UserResponse findAllOrByRole(UserRequest req) {
		List<User> userList = null;
		if(req == null) {
			userList = userRepository.findAll();
		} else {
			userList = userRepository.findByRole(req.getRole());
		}
		
		List<UserDto> dtoList = new ArrayList<>();
		if(userList != null) {
			for(User row : userList) {
				UserDto userMap = UserDto.builder()
						.userId(String.valueOf(row.getUserId()))
						.fullName(row.getFullName())
						.address(row.getAddress())
						.role(row.getRole())
						.build();
				dtoList.add(userMap);
			}
		}
		
		return UserResponse.builder().userList(dtoList).build();
	}
	
	public List<User> getUser(Long id) {
		return userRepository.findByUserId(id);
	}
	
	public User saveUser(User user) {
		return userRepository.save(user);
	}
	
	public void deleteUser(Long id) {
		userRepository.deleteById(id);
	}
}
