package com.bailram.usermanagementservice.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bailram.usermanagementservice.domain.dto.UserRequest;
import com.bailram.usermanagementservice.domain.dto.UserResponse;
import com.bailram.usermanagementservice.domain.entity.User;
import com.bailram.usermanagementservice.service.UserManagementService;

import lombok.extern.slf4j.Slf4j;

@Controller
@Slf4j
@RequestMapping("/api/usermanagement/v1")
public class UserManagementController {
	@Autowired
	private UserManagementService userManagementService;
	
	@PostMapping("/getAllUser")
	@ResponseBody
	public UserResponse getAllUser() {
		try {
			UserResponse result = userManagementService.findAllOrByRole(null);
			log.info("return all user list");
			return result;
		} catch (Exception e) {
			// TODO: handle exception
			log.error("Exception happened when get all user list message [{}]", e);
			throw e;
		}
	}
	
	@PostMapping("/getUserByRole")
	@ResponseBody
	public UserResponse getUserByRole(@RequestBody UserRequest request) {
		try {
			UserResponse result = userManagementService.findAllOrByRole(request);
			log.info("return all user by role");
			return result;
		} catch (Exception e) {
			// TODO: handle exception
			log.info("Exception happened when get all user list by role message [{}]", e);
			throw e;
		}
	}
	
	@PostMapping("/createUser")
	@ResponseBody
	public ResponseEntity<User> createUser(@RequestBody User user) {
		try {
			User insert = userManagementService.saveUser(user);
			return new ResponseEntity<>(insert, HttpStatus.CREATED);
		} catch (Exception e) {
			// TODO: handle exception
			log.info("\n error at -> " + e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@SuppressWarnings("rawtypes")
	@PostMapping("/deleteUser/{id}")
	public ResponseEntity deleteUser(@PathVariable("id") Long id) {
		try {
			log.info("id " + id);
			userManagementService.deleteUser(id);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(e,HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
