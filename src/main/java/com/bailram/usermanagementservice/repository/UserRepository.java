package com.bailram.usermanagementservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bailram.usermanagementservice.domain.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
	List<User> findByRole(String role);
	 
	List<User> findByUserId(Long userId);
}
